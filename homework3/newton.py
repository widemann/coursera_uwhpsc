# -*- coding: utf-8 -*-
"""
Created on Thu Apr 17 15:23:57 2014

@author: widemann
"""
from numpy import sqrt
def f(x,n):
    def g(y): return y**2 - 2.
    def gprime(y): return 2.*y
    for i in range(n):
        if x == 0:
            x = float('nan')
            print "division by zero error"
            break
        print "%0.15f" %(x)
        x = x - g(x)/gprime(x)
    print sqrt(2)
    return x    
    