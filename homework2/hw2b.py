
"""
Demonstration module for quadratic interpolation.
Update this docstring to describe your code.
Modified by: David Widemann
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve, cond
from sys import float_info

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """
    # check inputs and print error message if not valid:
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message
    # Set up linear system to interpolate through data points:
    A = np.vstack([np.ones(3), xi, xi**2]).T
    if cond(A) < 1./float_info.epsilon:
        ### Fill in this part to compute c ###
        c = solve(A,yi)
    else:
        print("matrix is singular")
        c = float('NaN')
    return c
    


def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([1., 2., 3.])
    yi = np.array([1., 2., 3.])
    c = quad_interp(xi,yi)
    c_true = np.array([0.,  1.,  0.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
        
def test_quad3():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([1., 1., 3.])
    yi = np.array([1., 2., 4.])
    c = quad_interp(xi,yi)
    print "xi =     ", xi
    print "c =      ", c
    # test that all elements have small error:
    assert np.isnan(c), \
        "Incorrect result, c = %s, Expected: c = NaN" % (c)        
        
def plot_quad(xi,yi):
    c = quad_interp(xi,yi)    
    x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line
    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles    
    plt.title("Data points and interpolating polynomial")
    plt.savefig('quadratic.png')       
        
def cubic_interp(xi,yi):
    """
    cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.

    """
    # check inputs and print error message if not valid:
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
    error_message = "xi and yi should have length 3"
    assert len(xi)==4 and len(yi)==4, error_message
    # Set up linear system to interpolate through data points:
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    if cond(A) < 1./float_info.epsilon:
        ### Fill in this part to compute c ###
        c = solve(A,yi)
    else:
        print("matrix is singular")
        c = float('NaN')
    return c

def plot_cubic(xi,yi):
    c = cubic_interp(xi,yi)    
    x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3
    #plt.ion()
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line
    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles    
    plt.title("Data points and interpolating polynomial")
    plt.savefig('cubic.png')      
    
def test_cubic1():
    xi = np.array([1., 2., 3.,5.])
    yi = np.array([1., -1., 3.,-10])
    c = cubic_interp(xi,yi)
    c_true = np.array([ 18.75 , -28.875,  12.75 ,  -1.625])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)    
        
def poly_interp(xi,yi):
    """
    polynomial interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3,...,n-1
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + ... + c[n-1]*x**(n-1).

    """
    # check inputs and print error message if not valid:
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
    error_message = "xi and yi should have the same length"
    n = len(xi)
    assert n==len(yi), error_message
    # Set up linear system to interpolate through data points:
    A = np.ones(n)
    for i in range(1,n):
        A = np.vstack((A,xi**i))
    A = A.T     
    if cond(A) < 1./float_info.epsilon:
        ### Fill in this part to compute c ###
        c = solve(A,yi)
    else:
        print("matrix is singular")
        c = float('NaN')
    return c    


def plot_poly(xi,yi):
    c = poly_interp(xi,yi)    
    x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    n = len(xi)
    y = c[n-1]
    for j in range(n-1, 0, -1):
        y = y*x + c[j-1]
    #plt.ion()
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line
    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles    
    plt.title("Data points and interpolating polynomial")
    plt.savefig('poly_interp.png')         
    
        
if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    print('\n')
    test_quad2()
    print('\n')
    test_quad3()
    print('\n')
    test_cubic1()







